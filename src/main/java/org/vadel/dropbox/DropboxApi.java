package org.vadel.dropbox;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.common.Helpers;
import org.vadel.dropbox.model.AccountInfo;
import org.vadel.dropbox.model.DropFile;
import org.vadel.dropbox.model.DropFolder;
import org.vadel.dropbox.model.Metadata;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@SuppressWarnings("unused")
public class DropboxApi implements DropboxApiInner.GetAccess {

	public enum RootEndPoint { sandbox, dropbox }

	final String clientId;
	final String redirectUri;

	private String accessToken;
	public String uid;

	private RootEndPoint root = RootEndPoint.dropbox;

	public final DropboxApiInner inner;

	public DropboxApi(String clientId, String redirectUri) {
		this.clientId = clientId;
		this.redirectUri = redirectUri;
		inner = new DropboxApiInner(this);
	}

	@Override
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken, String uid) {
		this.accessToken = accessToken;
		this.uid = uid;
	}

	@Override
	public RootEndPoint getEndPoint() {
		return root;
	}

	public void setEndPoint(RootEndPoint value) {
		this.root = value;
	}

	public String getAuthoriztionUrl() {
		return String.format(Consts.AUTHORIZE, clientId, redirectUri);
	}

	public void finishAuthoriztion(String uri) {
		if (uri == null)
			return;
			
		int i = uri.indexOf('?');
		if (i < 0) {
			i = uri.indexOf('#');
			if (i < 0) 
				return;
		}
			
		uri = uri.substring(i + 1);
		String[] values = uri.split("&");
		for (String value : values) {
			String[] param = value.split("=");
			if (param.length != 2)
				continue;
			if (param[0].equals("access_token"))
				this.accessToken = param[1];
			else if (param[0].equals("uid"))
				this.uid = param[1];
		}
	}

	public void signOff() {
		this.accessToken = null;
		this.uid = null;
	}

	public boolean isAuthenticated() {
		return this.accessToken != null;
	}

	/**
	 *
	 * @param path - Dropbox path
	 * @return Stream of binary (any!) file.
	 *
	 * curl -X POST https://content.dropboxapi.com/2/files/download \
	 *      --header "Authorization: Bearer <get access token>" \
	 *      --header "Dropbox-API-Arg: {\"path\": \"/Homework/math/Prime_Numbers.txt\"}"
	 */
	public InputStream getFileStream(String path) {
		if (!isAuthenticated())
			return null;
		try {
//			System.out.println("Stream path: " + path);
			JSONObject dropboxApiAgr = new JSONObject().put("path", path);
			return DropboxApiInner.getStreamFromUri(true, Consts.GET_FILES, accessToken, null, dropboxApiAgr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 *
	 * @param path - Dropbox path
	 * @return Text file.
	 *
	 * curl -X POST https://content.dropboxapi.com/2/files/download \
	 *      --header "Authorization: Bearer <get access token>" \
	 *      --header "Dropbox-API-Arg: {\"path\": \"/Homework/math/Prime_Numbers.txt\"}"
	 */
	public String getFileString(String path) {
		if (!isAuthenticated())
			return null;
		try {
			JSONObject dropboxApiAgr = new JSONObject().put("path", path);
			return DropboxApiInner.getStringFromUrl(true, Consts.GET_FILES, accessToken, null, dropboxApiAgr.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AccountInfo accountInfo() {
		if (!isAuthenticated())
			return null;
		JSONObject obj = inner.accountInfo();
		if (obj == null)
			return null;
		return AccountInfo.fromJSON(obj);
	}
	
	public String media(String path) {
		if (!isAuthenticated())
			return null;
		JSONObject obj = inner.media(path);
		if (obj == null || !obj.has("link"))
			return null;

		try {
			return obj.getString("link");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 *
	 * @param path
	 * @param loadAll - if true and return 'has_more'=true then continue load other files
	 * @return
	 */
	public DropFolder listFolder(String path, boolean loadAll) {
		JSONObject obj = inner.listFolder(path);
		if (obj == null)
			return null;
		DropFolder folder = new DropFolder();
		folder.path = path;
//			folder.isDir = true;
		folder.returnedAll = loadAll;
		folder.name = Helpers.getPageName(folder.path);
		try {
			boolean hasMore = obj.getBoolean("has_more");
			if (obj.has("entries")) {
				folder.loadItemsFromJSON(obj.getJSONArray("entries"));
			}
			if (hasMore){
				String cursor = obj.getString("cursor");
				if (loadAll) {
					DropFolder moreFolder;
					do {
						moreFolder = listFolderContinue(cursor);
						if (moreFolder != null) {
							folder.contents.addAll(moreFolder.contents);
							cursor = folder.cursor;
						}
					} while (moreFolder != null && moreFolder.hasMore);
				} else {
					folder.hasMore = hasMore;
					folder.cursor = cursor;
				}
			}
			return folder;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public DropFolder listFolderContinue(String cursor) {
		JSONObject obj = inner.listFolderContinue(cursor);
		if (obj == null)
			return null;
		DropFolder folder = new DropFolder();
//		folder.path = path;
//			folder.isDir = true;
//		folder.returnedAll = loadAll;
//		folder.name = Helpers.getPageName(folder.path);
		try {
			folder.hasMore = obj.getBoolean("has_more");
			folder.cursor = obj.getString("cursor");
			if (obj.has("entries")) {
				folder.loadItemsFromJSON(obj.getJSONArray("entries"));
			}
			return folder;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Metadata metadata(String path) {
		JSONObject obj = inner.metadata(path);
		if (obj == null)
			return null;
		return Metadata.fromJson(obj);
	}

	public static String encodePaths(String path) {
		StringBuilder str = new StringBuilder();
//		if (path.startsWith("/"))
//			str.append('/');
		try {
			for (String s : path.split("/")) {
				if (s.length() == 0)
					continue;
//				if (str.length() == 0 || str.charAt(str.length() - 1) != '/')
//					str.append('/');
				str.append('/').append(URLEncoder.encode(s, "utf-8").replace("+", "%20"));
			}
			if (path.endsWith("/"))
				str.append('/');
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str.toString();
	}
}
