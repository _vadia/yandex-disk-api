package org.vadel.dropbox.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by VBabin on 18.10.2017.
 */
public class DropFolder {

    public String id;
    public String path;
    public String name;
//    public boolean isDir;
//    public boolean isDeleted;
    public boolean hasMore;
    public String cursor;
    public boolean returnedAll;

    public final ArrayList<DropFile> contents = new ArrayList<DropFile>();


    public void loadItemsFromJSON(JSONArray entries) throws JSONException {
        for (int i = 0; i < entries.length(); i++) {
            contents.add(DropFile.fromJSON(entries.getJSONObject(i)));
        }
    }
}
