package org.vadel.dropbox.model;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountInfo {
	public String referralLink;
	public String displayName;
	public String abbreviatedName;
	public String accountId;
//	public String uid;
	public String email;
	public boolean emailVerified;
	public String country;
	public String locale;
//	public long quotaShared;
//	public long quota;
//	public long quotaNormal;
	public boolean disabled;
/*
	"name": {
			"given_name": "Вадим",
				"surname": "Бабин",
				"familiar_name": "Вадим",
				"": "Вадим Бабин",
	},

	"is_paired": false,
	"account_type": {
		".tag": "basic"
	}
*/
	@Override
	public String toString() {
		return "{ " + displayName + ", " + accountId + ", " + country + " }";
	}
	
	public static AccountInfo fromJSON(String s) {
		try {
			return fromJSON(new JSONObject(s));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static AccountInfo fromJSON(JSONObject obj) {
		try {
			AccountInfo account = new AccountInfo();
			account.referralLink = obj.getString("referral_link");
			account.accountId = obj.getString("account_id");

			JSONObject name = obj.getJSONObject("name");
			account.displayName = name.getString("display_name");
			account.abbreviatedName = name.getString("abbreviated_name");

			account.country = obj.getString("country");
			account.locale = obj.getString("locale");
			account.disabled = obj.getBoolean("disabled");
			if (obj.has("email")) {
				account.email = obj.getString("email");
				account.emailVerified = obj.getBoolean("email_verified");
			}
//			JSONObject quota = obj.getJSONObject("quota_info");
//			if (quota != null) {
//				account.quota = quota.getLong("quota");
//				account.quotaShared = quota.getLong("shared");
//				account.quotaNormal = quota.getLong("normal");
//			} else {
//				account.quota = 0l;
//				account.quotaShared = 0l;
//				account.quotaNormal = 0l;
//			}
			return account;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}}
