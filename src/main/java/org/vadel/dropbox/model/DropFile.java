package org.vadel.dropbox.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class DropFile {

	public boolean isDir;
	public String name;
	public String path;
	public String id;
	public String tag;
	public Date serverModified;
	public Date clientModified;
	public String rev;
	public long bytes;
	public String hash;

	/*
        "media_info": {
            ".tag": "metadata",
            "metadata": {
                ".tag": "photo",
                "dimensions": {
                    "height": 2480,
                    "width": 3508
                },
            "time_taken": "2015-12-04T15:41:05Z"
        }
    */
	public JSONObject media;

	public boolean isDeleted;

	public String getParent() {
		if (path == null || path.length() == 0 || path.equals("/"))
			return null;

//		int start = path.indexOf('/');
		int end   = path.lastIndexOf('/');
		int len   = path.length();
//		if (start == end)
//			return null;
		if (end == len - 1)
			end = path.lastIndexOf('/', end - 1);
		return path.substring(0, end);
	}

//	public String getParent() {
//		if (path == null || path.length() == 0 || path.equals("/"))
//			return null;
//
//		int i = path.lastIndexOf('/');
//		if(i < 0) {
//			return null;
//		} else {
//			if(i == path.length() - 1) {
//				i = path.lastIndexOf('/', i - 1);
//			}
//
//			return path.substring(0, i + 1);
//		}
//	}

	@Override
	public String toString() {
		return "{ " + (isDir ? "dir" : "file") + ": " + path + " - " + bytes + " }";
	}
	
	static SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

	@SuppressWarnings("deprecation")
	public static DropFile fromJSON(JSONObject obj) {
		try {
			DropFile file = new DropFile();
			file.tag = obj.getString(".tag");
			file.isDir = "folder".equals(file.tag);
			file.name = obj.getString("name");//Helpers.getPageName(file.path);
			file.path = obj.getString("path_display");
			file.isDeleted = obj.has("is_deleted") && obj.getBoolean("is_deleted");
			file.id = obj.getString("id");
			if (obj.has("rev"))
				file.rev = obj.getString("rev");
			if (obj.has("size"))
				file.bytes = obj.getLong("size");
			if (obj.has("content_hash"))
				file.hash = obj.getString("content_hash");
			if (obj.has("client_modified"))
				try {
					file.clientModified = dtf.parse(obj.getString("client_modified"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			if (obj.has("server_modified"))
				try {
					file.serverModified = dtf.parse(obj.getString("server_modified"));
				} catch (ParseException e) {
					e.printStackTrace();
				}

			if (obj.has("media_info"))
				file.media = obj.getJSONObject("media_info");
//			public boolean isDeleted;
			return file;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
