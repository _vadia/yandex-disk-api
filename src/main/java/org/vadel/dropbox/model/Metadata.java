package org.vadel.dropbox.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VBabin on 19.10.2017.
 */
public class Metadata {

    public String tag;
    public boolean isDir;
    public String name;
    public String path;
    public String id;

    public static Metadata fromJson(JSONObject obj) {
        try {
            Metadata metadata = new Metadata();
            metadata.tag = obj.getString(".tag");
            metadata.isDir = "folder".equals(metadata.tag);
            metadata.name = obj.getString("name");
            metadata.path = obj.getString("path_display");
            metadata.id = obj.getString("id");
            return metadata;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
