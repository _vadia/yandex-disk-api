package org.vadel.dropbox;

import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.common.Helpers;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Created by VBabin on 18.10.2017.
 */
public class DropboxApiInner {

    private final GetAccess getAccess;

    public DropboxApiInner(GetAccess getAccess) {
        this.getAccess = getAccess;
    }

    public JSONObject accountInfo() {
        return accountInfo(getAccess.getAccessToken());
    }

    public JSONObject listFolder(String path) {
        return listFolder(getAccess.getAccessToken(), path);
    }

    public JSONObject listFolderContinue(String cursor) {
        return listFolderContinue(getAccess.getAccessToken(), cursor);
    }

    public JSONObject media(String path) {
        return media(getAccess.getAccessToken(), path);
    }

    public JSONObject metadata(String path) {
        return metadata(getAccess.getAccessToken(), path);
    }

    /**
     *
     * @param accessToken
     * @return
     *
     * curl -X POST https://api.dropboxapi.com/2/users/get_current_account \
     *      --header "Authorization: Bearer <get access token>"
     */
    public static JSONObject accountInfo(String accessToken) {
        String s = getStringFromUrl(true, Consts.ACCOUNT, accessToken, null, null);
        if (s == null)
            return null;
        try {
            return new JSONObject(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * curl -X POST https://api.dropboxapi.com/2/files/get_metadata \
     *      --header "Authorization: Bearer <get access token>" \
     *      --header "Content-Type: application/json" \
     *      --data "{\"path\": \"/Homework/math\",\"include_media_info\": false,\"include_deleted\": false,\"include_has_explicit_shared_members\": false}"
     */
    public static JSONObject metadata(String accessToken, String path) {
		if (path.equals("/"))
			path = "";

		try {
			JSONObject data = new JSONObject()
					.put("path", path)
					.put("include_media_info", false)
					.put("include_has_explicit_shared_members", false)
					.put("include_deleted", false);
			String s = getStringFromUrl(true, Consts.METADATA, accessToken, data.toString(), null);
			if (s == null)
				return null;
            return new JSONObject(s);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

    /**
     * example:
     *
     * curl -X POST https://api.dropboxapi.com/2/files/list_folder \
     *      --header "Authorization: Bearer <get access token>" \
     *      --header "Content-Type: application/json" \
     *      --data "{\"path\": \"/Homework/math\",\"recursive\": false,\"include_media_info\": false,\"include_deleted\": false,\"include_has_explicit_shared_members\": false,\"include_mounted_folders\": true}"    */
    public static JSONObject listFolder(String accessToken, String path) {
        if (path.equals("/"))
            path = "";

        try {
            JSONObject data = new JSONObject()
                    .put("path", path)
                    .put("include_media_info", true)
                    .put("include_has_explicit_shared_members", false)
                    .put("recursive", false)
                    .put("include_deleted", false)
                    .put("include_mounted_folders", true);
            String s = getStringFromUrl(true, Consts.LIST_FOLDER, accessToken, data.toString(), null);
            if (s == null)
                return null;
            return new JSONObject(s);//
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject listFolderContinue(String accessToken, String cursor) {
        try {
            JSONObject data = new JSONObject().put("cursor", cursor);
            String s = getStringFromUrl(true, Consts.LIST_FOLDER_CONTINUE, accessToken, data.toString(), null);
            if (s == null)
                return null;
            return new JSONObject(s);//
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * example:
     *
     *		curl -X POST https://api.dropboxapi.com/2/files/get_temporary_link \
     *				--header "Authorization: Bearer <get access token>" \
     *				--header "Content-Type: application/json" \
     *				--data "{\"path\": \"/video.mp4\"}"
     */
    public static JSONObject media(String accessToken, String path) {
        try {
            JSONObject data = new JSONObject().put("path", path);
            String s = getStringFromUrl(true, Consts.MEDIA, accessToken, data.toString(), null);
            if (s == null)
                return null;
            return new JSONObject(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStringFromUrl(boolean post, String uri, String accessToken, String data, String dropboxApiArg) {
//        InputStream in = getStreamFromUri(post, uri, accessToken, data, dropboxApiArg);
//        if (in == null)
//            return null;
//        try {
//            StringBuilder str = new StringBuilder();
//            int n;
//            char[] buff = new char[512];
//            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//            while((n = reader.read(buff)) >= 0) {
//                str.append(buff, 0, n);
//            }
//            reader.close();
//            return str.toString();
//        } catch (MalformedURLException e) {
//            System.out.println("Error! Can't download uri: " + uri);
//            e.printStackTrace();
//        } catch (IOException e) {
//            System.out.println("Error! Can't download uri: " + uri);
//            e.printStackTrace();
//        }
        return getStreamToString(getStreamFromUri(post, uri, accessToken, data, dropboxApiArg));
    }

    public static InputStream getStreamFromUri(boolean post, String uri, String accessToken, String data, String dropboxApiArg) {
        try {
            URL Url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
            if (conn == null)
                return null;
            if (post)
                conn.setRequestMethod("POST");
            HttpURLConnection.setFollowRedirects(true);
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if (accessToken != null) {
                // --header "Authorization: Bearer <get access token>" \
                conn.setRequestProperty("Authorization", "Bearer " + accessToken);
            }
            conn.setRequestProperty("Content-Type", "");
            if (data != null) {
                conn.setRequestProperty("Content-Type", "application/json");
//                System.out.println(data);
                conn.setDoOutput(true);
                // --header "Content-Type: application/json"
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
            }
            if (dropboxApiArg != null) {
//                conn.setRequestProperty("Content-Type", "text/plain");
//                System.out.println(dropboxApiArg);
                // --header "Dropbox-API-Arg: {\"path\": \"/Homework/math/Prime_Numbers.txt\"}"
                conn.setRequestProperty("Dropbox-API-Arg", dropboxApiArg);
            }
            if (conn.getResponseCode() >= 400) {
                String err = getStreamToString(getInputStreamToEncoding(conn.getErrorStream(), conn.getContentEncoding()));
                System.out.println("Error: " + err);
                return null;
            } else {
                return getInputStreamToEncoding(conn.getInputStream(), conn.getContentEncoding());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

//    public static InputStream getInputEncoding(HttpURLConnection connection) throws IOException {
//        InputStream in;
//        String encoding = connection.getContentEncoding();
//        if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
//            in = new GZIPInputStream(connection.getInputStream());
//        } else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
//            in = new InflaterInputStream(connection.getInputStream(), new Inflater(true));
//        } else {
//            if (connection.getResponseCode() >= 400) {
//                in = connection.getErrorStream();
//            } else {
//                in = connection.getInputStream();
//            }
//        }
//        return in;
//    }

    public static String getStreamToString(InputStream in) {
        if (in == null)
            return null;
        try {
            StringBuilder str = new StringBuilder();
            int n;
            char[] buff = new char[512];
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while((n = reader.read(buff)) >= 0) {
                str.append(buff, 0, n);
            }
            reader.close();
            return str.toString();
        } catch (MalformedURLException e) {
//            System.out.println("Error! Can't download uri: " + uri);
            e.printStackTrace();
        } catch (IOException e) {
//            System.out.println("Error! Can't download uri: " + uri);
            e.printStackTrace();
        } finally {
            Helpers.closeQuietly(in);
        }
        return null;
    }


    public static InputStream getInputStreamToEncoding(InputStream in, String encoding) throws IOException {
        if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
            in = new GZIPInputStream(in);
        } else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
            in = new InflaterInputStream(in, new Inflater(true));
//        } else {
//            in = connection.getInputStream();
        }
        return in;
    }

    public interface GetAccess {
        String getAccessToken();
        DropboxApi.RootEndPoint getEndPoint();
    }
}
