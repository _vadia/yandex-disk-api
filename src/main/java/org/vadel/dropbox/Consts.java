package org.vadel.dropbox;

/**
 * Created by VBabin on 19.10.2017.
 */
public class Consts {

    static final String AUTHORIZE = "https://www.dropbox.com/oauth2/authorize?response_type=token&client_id=%s&redirect_uri=%s";

    static final String ACCOUNT     = "https://api.dropbox.com/2/users/get_current_account";
    static final String GET_FILES   = "https://content.dropboxapi.com/2/files/download";
    static final String PUT_FILES   = "https://api-content.dropbox.com/2/files/upload";
    static final String METADATA    = "https://api.dropbox.com/2/files/get_metadata";
    static final String LIST_FOLDER = "https://api.dropbox.com/2/files/list_folder";
    static final String LIST_FOLDER_CONTINUE = LIST_FOLDER + "/continue";
    static final String MEDIA       = "https://api.dropbox.com/2/files/get_temporary_link";

}
