package org.vadel.cloudmailru;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vbabin on 23.05.2018.
 */
public class Cookies {
    public Map<String, String> values = new HashMap<String, String>();

    public String getCookie() {
        StringBuilder str = new StringBuilder();
        for (String name : values.keySet()) {
            if (str.length() > 0)
                str.append("; ");
            str.append(name).append('=').append(values.get(name));
        }
        return str.toString();
    }

    public void setCookie(String s) {
        if (s == null)
            return;
        for (String vals : s.split("; ")) {
            String[] ss = vals.trim().split("=");
            if (ss.length == 2) {
                values.put(ss[0], ss[1]);
            } else {
                values.put(ss[0], null);
            }
        }
    }

    public void setCookie(List<String> l) {
        if (l == null)
            return;
        for (String s : l) {
            String[] vals = s.split(";");
            String[] ss = vals[0].trim().split("=");
            String nm = ss[0];
            String vl = null;
            if (ss.length == 2) {
                vl = ss[1];
            }
            System.out.println("Set-Cookie: " + nm + "=" + vl);
            values.put(nm, vl);
        }
    }

    public void setToConnection(HttpURLConnection conn) {
        if (values.size() == 0)
            return;
        conn.setRequestProperty("Cookie", getCookie());
    }
}
