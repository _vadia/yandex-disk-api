package org.vadel.cloudmailru;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.common.Helpers;
import org.vadel.libs.connect.Connect;
import org.vadel.libs.connect.Request;
import org.vadel.libs.connect.Response;
import org.vadel.yandexdisk.OnLoadProgressListener;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;

/**
 * Created by vbabin on 23.05.2018.
 */
public class CloudMailRuApi {

    public long chunkSize = 100*1024;

    private String username;
    private String password;
    private String domain;
    private String accessToken;

    public final Cookies cookies = new Cookies();

    public String getEmail() {
        if (username == null || domain == null)
            return null;
        return username + "@" + domain;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDomain() {
        return domain;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAuth(String login, String password, String accessToken) {
        String[] ss = login.split("@");
        if (ss.length != 2)
            return;
        setAuth(ss[0], password, ss[1], accessToken);
    }

    public void setAuth(String username, String password, String domain, String accessToken) {
        this.username = username;
        this.password = password;
        this.domain = domain;
        this.accessToken = accessToken;
    }

    public void signOff() {
        this.cookies.values.clear();
        this.accessToken = null;
    }

    public boolean isAuthenticated() {
        return this.accessToken != null;
    }

    public boolean authorize(String login, String password) throws IOException {
        if (login == null || password == null) {
            return false;
        }
        String[] ss = login.split("@");
        if (ss.length != 2)
            return false;
        return authorize(ss[0], password, ss[1]);
    }

    public boolean authorize(String username, String password, String domain) throws IOException {
        this.username = null;
        this.password = null;
        this.domain = null;
        if (!requestLogin(username, password, domain)) {
            System.err.println("Invalid username or password");
            return false;
        }
//        requestSdcCookie();
        try {
            this.accessToken = requestAccessToken();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isAuthenticated()) {
            this.username = username;
            this.password = password;
            this.domain = domain;
        }
        return accessToken != null;
    }

    protected boolean requestLogin(String username, String password, String domain) throws IOException {
        String page = "https://cloud.mail.ru/home/?from=promo";
        HttpURLConnection.setFollowRedirects(false);
        HttpsURLConnection.setFollowRedirects(false);
        Response response = Connect.post(Consts.AUTH_URI)
                .asChrome()
                .withHeader("Content-Type", Consts.defaultRequestType)
                .withHeader("Accept", Consts.defaultAcceptType)
                .withHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
                .withHeader("Host", "auth.mail.ru")
                .withHeader("Origin", "https://cloud.mail.ru")
                .withHeader("Referer", Consts.REFERER)
                .addDataForm("Login", username)
                .addDataForm("Domain", domain)
                .addDataForm("Password", password)
                .addDataForm("page", page)
                .addDataForm("saveauth", "1")
                .withFollowRedirect(false)
                .execute();

        String redirectTo = response.getHeaderFirst("Location");
        if (response.getCode() >= 400 || redirectTo == null || !redirectTo.equals(page)) {
            return false;
        }
        System.out.println("Res: " + response.getCode() + " --> " + redirectTo);
        cookies.setCookie(response.getSetCookiesList());

        while (redirectTo != null) {
            response = reqChrome(Connect.get(redirectTo).withFollowRedirect(false));

            cookies.setCookie(response.getSetCookiesList());
            redirectTo = response.getHeaderFirst("Location");
            System.out.println("Res: " + response.getCode() + " --> " + redirectTo);
        }
        return true;
    }

//    protected boolean requestSdcCookie() throws IOException {
//        Response response = Connect.get("https://auth.mail.ru/sdc")//Consts.AUTH_URI + "/sdc")//?from=" + URLEncoder CLOUD_DOMAIN + "/home";)
//                .addQuery("from", Consts.CLOUD_DOMAIN + "/home")
//                .asChrome()
//                .withHeader("Content-Type", Consts.defaultRequestType)
//                .withHeader("Accept", Consts.defaultAcceptType)
//                .withHeader("Host", "cloud.mail.ru")
//                .withHeader("Referer", "https://cloud.mail.ru/home/")
//                .withCookies(cookies.values)
//                .withFollowRedirect(false)
//                .execute();
//        System.out.println("Res: " + response.getCode() + " --> " + response.getHeader("Location"));
//        cookies.setCookie(response.getSetCookiesList());
//        return response.isOk();
//    }

    protected Response reqChrome(Request request) throws IOException {
        return request
                .asChrome()
                .withHeaderIf("Content-Type", Consts.defaultRequestType)
                .withHeaderIf("Accept", Consts.defaultAcceptType)
                .withHeaderIf("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
                .withHeaderIf("Referer", Consts.REFERER)
                .withCookies(cookies.values)
//                .withFollowRedirect(false)
                .execute();
    }

    protected JSONObject reqJson(Request request) throws IOException, JSONException {
        return request.asChrome()
//                .addQuery("token", accessToken)
                .withHeaderIf("Content-Type", Consts.defaultRequestType)
                .withHeaderIf("Accept", Consts.APPLICATION_JSON)
                .withCookies(cookies.values)
                .execute()
                .asJsonIfOk();
    }

    protected String requestAccessToken() throws IOException, JSONException {
        JSONObject o = reqJson(Connect.get(Consts.ACCESS_TOKEN_URI));
        if (o == null) {
            return null;
        }
        return Helpers.getJson(o, String.class, "body", "token");
    }

    public JSONObject diskSpace() throws IOException {
        try {
            return Helpers.getJson(
                    reqJson(Connect.get(Consts.SPACE_URI)
                        .addQuery("api", "2")
                        .addQuery("email", getEmail())
                        .addQuery("token", accessToken)),
                    JSONObject.class, "body");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
/*
        var uri = new Uri(string.Format("{0}/api/v2/user/space?api=2&email={1}&token={2}", ConstSettings.CloudDomain, this.LoginName, this.AuthToken));
            var request = (HttpWebRequest)WebRequest.Create(uri.OriginalString);
            request.Proxy = this.Proxy;
            request.CookieContainer = this.Cookies;
            request.Method = "GET";
            request.ContentType = ConstSettings.DefaultRequestType;
            request.Accept = "application/json";
            request.UserAgent = ConstSettings.UserAgent;
            var task = Task.Factory.FromAsync(request.BeginGetResponse, asyncResult => request.EndGetResponse(asyncResult), null);
            return await task.ContinueWith((t) =>
            {
                using (var response = t.Result as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return JsonParser.Parse(new MailRuCloud().ReadResponseAsText(response), PObject.DiskUsage) as DiskUsage;
                    }
                    else
                    {
                        throw new Exception("The disk usage statistic can't be retrieved.");
                    }
                }
            });
         */
    }

    public boolean createFolder(String path) throws IOException {
        return Connect.post(Consts.ADD_FOLDER_URI)
                .asChrome()
                .withHeader("Referer", Helpers.addBackSlash(Consts.CLOUD_DOMAIN))
                .withHeader("Accept", "*/*")
                .withCookies(cookies.values)
                .addDataForm("home", path)
                .addDataForm("api", "2")
                .addDataForm("token", accessToken)
                .addDataForm("conflict", "rename")
                .execute()
                .isOk();
    }

    public boolean remove(String path) throws IOException {
        return Connect.post(Consts.REMOVE_URI)
                .asChrome()
                .addDataForm("home", path)
                .addDataForm("api", "2")
                .addDataForm("token", accessToken)
                .addDataForm("email", getEmail())
                .addDataForm("x-email", getEmail())
                .withCookies(cookies.values)
                .withHeader("Referer", Helpers.getFolderPath(path))
                .withHeader("Origin", Consts.CLOUD_DOMAIN)
                .withHeader("Accept", "*/*")
                .withHeader("Content-Type", Consts.defaultRequestType)
                .execute()
                .isOk();
    }

    @SuppressWarnings("unused")
    public boolean copyTo(String srcPath, String dstFolder) throws IOException {
        return moveOrCopyTo(srcPath, dstFolder, false);
    }

    @SuppressWarnings("unused")
    public boolean moveTo(String srcPath, String dstFolder) throws IOException {
        return moveOrCopyTo(srcPath, dstFolder, true);
    }

    private boolean moveOrCopyTo(String srcPath, String dstFolder, boolean move) throws IOException {
        return reqChrome(Connect.post(move ? Consts.MOVE_URI : Consts.COPY_URI)
                        .addDataForm("home", srcPath)
                        .addDataForm("folder", dstFolder)
                        .addDataForm("conflict", "rename")
                        .addDataForm("api", "2")
                        .addDataForm("token", accessToken)
                        .addDataForm("email", getEmail())
                        .addDataForm("x-email", getEmail())
                        .withHeader("Origin", Consts.CLOUD_DOMAIN)
                        .withHeader("Referer", Helpers.addBackSlash(Consts.HOME_CLOUD_DOMAIN, Helpers.getFolderPath(srcPath)))
        ).isOk();
    }

    public boolean rename(String path, String newName) throws IOException {
        return reqChrome(Connect.post(Consts.RENAME_URI)
                        .addDataForm("home", path)
                        .addDataForm("name", newName)
                        .addDataForm("conflict", "rename")
                        .addDataForm("api", "2")
                        .addDataForm("token", accessToken)
                        .addDataForm("email", getEmail())
                        .addDataForm("x-email", getEmail())
                        .withHeader("Origin", Consts.CLOUD_DOMAIN)
                        .withHeader("Referer", Helpers.addBackSlash(Consts.HOME_CLOUD_DOMAIN, Helpers.getFolderPath(path)))
                ).isOk();
    }

    public String root() {
        return "/";
    }

    public JSONArray items(String path) {
        if (!isAuthenticated()) {
            return null;
        }
        try {
            JSONObject o = reqJson(Connect.get(Consts.ITEMS_URI)
                    .addQuery("token", accessToken)
                    .addQuery("home", path));
            if (o == null)
                return null;
            return Helpers.getJson(o, JSONArray.class, "body", "list");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String makePublic(String path) throws UnsupportedEncodingException {
        try {
            Response response = reqChrome(Connect.post(Consts.PUBLISH_URI)
                    .withHeader("Referer", Helpers.addBackSlash(Consts.CLOUD_DOMAIN, path))
                    .withHeader("Accept", "*/*")
                    .addDataForm("home", path)
                    .addDataForm("api", "2")
                    .addDataForm("token", accessToken)
                    .addDataForm("email", getEmail())
                    .addDataForm("x-email", getEmail()));

            if (!response.isOk()) {
                return null;
            }
            String code = Helpers.getJson(response.asJson(), String.class, "body");
            return Consts.PUBLISH_FILE_LINK + code;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean makeUnpublic(String publishLink, String path) {
        try {
            return reqChrome(Connect.post(Consts.UNPUBLISH_URI)
                    .withHeader("Referer", Helpers.addBackSlash(Consts.CLOUD_DOMAIN, path))
                    .withHeader("Accept", "*/*")
                    .addDataForm("weblink", publishLink.replace(Consts.PUBLISH_FILE_LINK, ""))
                    .addDataForm("api", "2")
                    .addDataForm("token", accessToken)
                    .addDataForm("email", getEmail())
                    .addDataForm("x-email", getEmail()))
                    .isOk();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getDownloadLink(String publishLink) throws IOException {
        if (!isAuthenticated()) {
            return null;
        }
        try {
            String url = getShardInfo(Consts.SHARD_WEBLINK_GET);
            if (url == null)
                return null;

            JSONObject o = Connect.post(Consts.DOWNLOAD_URI)
                    .asChrome()
                    .withHeader("Host", "cloud.mail.ru")
                    .withHeader("Origin", Consts.CLOUD_DOMAIN)
                    .withHeader("Referer", publishLink)
                    .withHeader("Content-Type", Consts.defaultRequestType)
                    .addDataForm("api", "2")
                    .execute()
                    .asJsonIfOk();
            String token = Helpers.getJson(o, String.class, "body", "token");
            if (token == null)
                return null;
            return Helpers.addBackSlash(url, publishLink.replace(Consts.PUBLISH_FILE_LINK, "")) + "?key=" + token;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream getDownloadStream(String path) throws IOException {
        if (!isAuthenticated()) {
            return null;
        }

        String url = getShardInfo(Consts.SHARD_GET);
        if (url == null) {
            return null;
        }
        Response response = reqChrome(Connect.get(Helpers.addBackSlash(url, path)));
        if (!response.isOk()) {
            return null;
        }
        return response.getInputEncoding();
    }

    @SuppressWarnings("unused")
    public String getFileString(String path, String charset) throws IOException {
        if (!isAuthenticated()) {
            return null;
        }
        InputStream in = getDownloadStream(path);
        if (in == null) {
            return null;
        }
        return Helpers.getStringFromStream(in, charset);
    }

    @SuppressWarnings("unused")
    public long download(String path, File dst, OnLoadProgressListener listener) throws IOException, InterruptedException {
        if (!isAuthenticated()) {
            return 0L;
        }
        InputStream in = getDownloadStream(path);
        if (in == null) {
            return 0L;
        }
        FileOutputStream out = new FileOutputStream(dst);
        return Helpers.copy(in, out, chunkSize, listener);
    }

    @SuppressWarnings("unused")
    public boolean upload(String path, StringBuilder data) throws IOException {
        return upload(path, data, null, data.toString().getBytes().length);
    }

    @SuppressWarnings("unused")
    public boolean upload(String path, InputStream in, long size) throws IOException {
        return upload(path, null, in, size);
    }

    @SuppressWarnings("unused")
    public boolean upload(String path, File file) throws IOException {
        FileInputStream in = new FileInputStream(file);
        return upload(path, null, in, file.length());
    }

    private boolean upload(String path, StringBuilder data, InputStream in, long size) throws IOException {
        String url = getShardInfo(Consts.SHARD_UPLOAD);
        String folderPath = Helpers.getFolderPath(path);

        Response response = Connect.put(url)
                .addQuery("cloud_domain", "2").addQuery("x-email", getEmail())
                .asChrome()
                .withHeader("Accept", "*/*")
                .withHeader("Referer", Helpers.addBackSlash(Consts.HOME_CLOUD_DOMAIN, folderPath))
                .withHeader("Origin", Consts.CLOUD_DOMAIN)
                .withHeader("Content-Type", "multipart/form-data")//"text/plain")//string.Format(" multipart/form-data; boundary=----{0}", boundary.ToString());)
                .withCookies(cookies.values)
                .withData(data)
                .withData(in)
                .execute();
        ;
        if (!response.isOk()) {
            return false;
        }

        StringBuilder hash = response.asString();
        if (hash == null)
            return false;
        return Connect.post(Consts.ADD_FILE_URI)
                .asChrome()
                .withHeader("Referer", Helpers.addBackSlash(Consts.CLOUD_DOMAIN, folderPath))
                .withHeader("Accept", "*/*")
                .withCookies(cookies.values)
                .addDataForm("home", path)
                .addDataForm("hash", hash.toString())
                .addDataForm("size","" + size)
                .addDataForm("api", "2")
                .addDataForm("token", accessToken)
                .addDataForm("conflict", "rename")
                .addDataForm("email", getEmail())
                .addDataForm("x-email", getEmail())
                .execute()
                .isOk();
    }

    protected String getShardInfo(String shardType) {
        if (shardType == null)
            return null;
        try {
            JSONObject obj = reqJson(Connect.get(Consts.SHARD_URI)
                    .addQuery("token", accessToken));
            if (obj == null)
                return null;
            JSONArray res = Helpers.getJson(obj, JSONArray.class, "body", shardType);
            if (res == null || res.length() == 0)
                return null;
            return Helpers.getJson(res.getJSONObject(0), String.class, "url");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
