package org.vadel.cloudmailru;

/**
 * Created by vbabin on 23.05.2018.
 */
public class Consts {

    public static final String SHARD_GET = "get";
    public static final String SHARD_UPLOAD = "upload";
    public static final String SHARD_WEBLINK_GET = "weblink_get";

    public static final String REFERER = "https://cloud.mail.ru/home/";

    public static final String DOMAIN = "mail.ru";
    public static final String CLOUD_DOMAIN = "https://cloud.mail.ru";
    public static final String HOME_CLOUD_DOMAIN = CLOUD_DOMAIN + "/home";
    public static final String AUTH_DOMEN = "https://auth.mail.ru";
    public static final String PUBLISH_FILE_LINK = CLOUD_DOMAIN + "/public/";

    public static final String AUTH_URI = AUTH_DOMEN + "/cgi-bin/auth";
    public static final String ACCESS_TOKEN_URI = CLOUD_DOMAIN + "/api/v2/tokens/csrf";
    public static final String SDC_COOKIE_URI = AUTH_URI + "/sdc?from=" + CLOUD_DOMAIN + "/home";
    public static final String SHARD_URI = Consts.CLOUD_DOMAIN + "/api/v2/dispatcher";
    public static final String ITEMS_URI = Consts.CLOUD_DOMAIN + "/api/v2/folder";
    public static final String DOWNLOAD_URI = CLOUD_DOMAIN + "/api/v2/tokens/download";
    public static final String PUBLISH_URI = CLOUD_DOMAIN + "/api/v2/file/publish";
    public static final String UNPUBLISH_URI = CLOUD_DOMAIN + "/api/v2/file/unpublish";
    public static final String ADD_FOLDER_URI = CLOUD_DOMAIN + "/api/v2/folder/add";
    public static final String ADD_FILE_URI = CLOUD_DOMAIN + "/api/v2/file/add";
    public static final String REMOVE_URI = CLOUD_DOMAIN + "/api/v2/file/remove";
    public static final String COPY_URI = CLOUD_DOMAIN + "/api/v2/file/copy";//, ConstSettings.CloudDomain, move ? "move" : "copy"));
    public static final String MOVE_URI = CLOUD_DOMAIN + "/api/v2/file/move";//, ConstSettings.CloudDomain, move ? "move" : "copy"));
    public static final String RENAME_URI = CLOUD_DOMAIN + "/api/v2/file/rename";//, ConstSettings.CloudDomain, move ? "move" : "copy"));
    public static final String SPACE_URI = CLOUD_DOMAIN + "/api/v2/user/space";//?api=2&email={1}&token={2}", ConstSettings.CloudDomain, this.LoginName, this.AuthToken));

    public static final String userAgent = "Mozilla / 5.0(Windows; U; Windows NT 5.1; en - US; rv: 1.9.0.1) Gecko / 2008070208 Firefox / 3.0.1";
    public static final String defaultAcceptType = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
    public static final String APPLICATION_JSON = "application/json";
    public static final String defaultRequestType = "application/x-www-form-urlencoded";

}
