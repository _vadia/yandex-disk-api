package org.vadel.common;

import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.yandexdisk.OnLoadProgressListener;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Created by vadimbabin on 24.04.14.
 */
public class Helpers {

    public static String getFolderPath(String path) {
        return path.substring(0, path.lastIndexOf('/') + 1);
    }

    static  <T> T getObj(JSONObject o, Class<T> type, String name) throws JSONException {
        if (o == null || !o.has(name))
            return null;
        Object res = o.get(name);
        if (type.isInstance(res))
            return (T) res;
        return null;
    }

    public static <T> T getJson(JSONObject obj, Class<T> type, String...path) throws JSONException {
        for (int i = 0; i < path.length - 1; i++) {
            String p = path[i];
            obj = getObj(obj, JSONObject.class, p);
            if (obj == null)
                return null;
        }
        return getObj(obj, type, path[path.length - 1]);
    }

    public static String addBackSlash(String s) {
        if (s.endsWith("/"))
            return s;
        else
            return s + "/";
    }

    public static String addBackSlash(String s, String s2) {
        boolean e0 = s.endsWith("/");
        boolean e2 = s2.startsWith("/");
        if (!e0 && !e2) {
            return s + "/" + s2;
        } else if (e0 ^ e2) {
            return s + s2;
        } else {
            return s + s2.substring(1);
        }
    }

    /**
     * The masks used to validate and parse the input to this Atom date. These
     * are a lot more forgiving than what the Atom spec allows. The forms that
     * are invalid according to the spec are indicated.
     */
    private static final String[] masksAtom = {
            "yyyy-MM-dd'T'HH:mm:ss.SSSz",
            "yyyy-MM-dd't'HH:mm:ss.SSSz", // invalid
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd't'HH:mm:ss.SSS'z'", // invalid
            "yyyy-MM-dd'T'HH:mm:ssz", "yyyy-MM-dd't'HH:mm:ssz", // invalid
            "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd't'HH:mm:ss'z'", // invalid
            "yyyy-MM-dd'T'HH:mmz", // invalid
            "yyyy-MM-dd't'HH:mmz", // invalid
            "yyyy-MM-dd'T'HH:mm'Z'", // invalid
            "yyyy-MM-dd't'HH:mm'z'", // invalid
            "yyyy-MM-dd", "yyyy-MM", "yyyy"};

    /**
     * Parse the serialized string form into a java.util.Date
     *
     * @param date The serialized string form of the date
     * @return The created java.util.Date
     */
    public static long parseAtomDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat();
        for (String s : masksAtom) {
            try {
                sdf.applyPattern(s);
                return sdf.parse(date).getTime();
            } catch (Exception e) {
            }
        }
        return 0;
    }

    public static int TIMEOUT_CONNECTION = 20000;
    public static final String CHROME_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";

    public static InputStream getStreamFromUri(String uri, OnStreamContentListener listener) {
        try {
            URL Url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
            if (conn == null)
                return null;
            HttpURLConnection.setFollowRedirects(true);
            conn.setReadTimeout(TIMEOUT_CONNECTION);
            conn.setConnectTimeout(TIMEOUT_CONNECTION);
            conn.setRequestProperty("User-Agent", CHROME_USER_AGENT);
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if (listener != null) {
                listener.setRequests(conn);
            }
            if (listener != null) {
                if (!listener.correctContentType(conn.getContentType()))
                    return null;
            }

//			if (conn instanceof HttpsURLConnection) {
//				HttpsURLConnection https = (HttpsURLConnection) conn;
//				System.out.println("Response(https): " + https.getResponseCode());
//			} else if (conn instanceof HttpURLConnection) {
//	  			HttpURLConnection http = (HttpURLConnection) conn;
//	  			System.out.println("Response(http): " + http.getResponseCode());
//			}
            InputStream in = getInputEncoding(conn);
//	  		for (String key : conn.getHeaderFields().keySet())
//	  			System.out.println(key + " : " + conn.getHeaderField(key));
            return in;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface OnStreamContentListener {
        void setRequests(URLConnection conn);

        boolean correctContentType(String type);
    }

    /**
     * получение имени страницы из ссылки (http://manga.com/manga1/chapter1.htm -> chapter1.htm)
     *
     * @param link - исходная ссылка, (http://manga.com/manga1/chapter1.htm)
     * @return название страницы, (chapter1.htm)
     */
    public static String getPageName(String link, boolean cutQueries) {
        String s = link.trim();
        if (s.endsWith("/")) {
            s = s.substring(0, s.length() - 1);
        }
        int i2 = -1;
        if (cutQueries)
            i2 = link.indexOf('?');
        if (i2 < 0)
            i2 = s.length();

        int i1 = s.lastIndexOf('/', i2) + 1;
//		int i2 = -1;
//		if (cutQueries)
//			i2 = link.indexOf('?');
//		if (i2 < 0)
//			i2 = s.length();
        if (i1 < 0)
            i1 = 0;
        return s.substring(i1, i2);
    }

    public static String getPageName(String link) {
        return getPageName(link, false);
    }

    private final static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
            .toCharArray();

    private static int[] toInt = new int[128];

    static {
        for (int i = 0; i < ALPHABET.length; i++) {
            toInt[ALPHABET[i]] = i;
        }
    }

    /**
     * Translates the specified byte array into Base64 string.
     *
     * @param buf the byte array (not null)
     * @return the translated Base64 string (not null)
     */
    public static String encode(byte[] buf) {
        int size = buf.length;
        char[] ar = new char[((size + 2) / 3) * 4];
        int a = 0;
        int i = 0;
        while (i < size) {
            byte b0 = buf[i++];
            byte b1 = (i < size) ? buf[i++] : 0;
            byte b2 = (i < size) ? buf[i++] : 0;

            int mask = 0x3F;
            ar[a++] = ALPHABET[(b0 >> 2) & mask];
            ar[a++] = ALPHABET[((b0 << 4) | ((b1 & 0xFF) >> 4)) & mask];
            ar[a++] = ALPHABET[((b1 << 2) | ((b2 & 0xFF) >> 6)) & mask];
            ar[a++] = ALPHABET[b2 & mask];
        }
        switch (size % 3) {
            case 1:
                ar[--a] = '=';
            case 2:
                ar[--a] = '=';
        }
        return new String(ar);
    }

    /**
     * Translates the specified Base64 string into a byte array.
     *
     * @param s the Base64 string (not null)
     * @return the byte array (not null)
     */
    public static byte[] decode(String s) {
        int delta = s.endsWith("==") ? 2 : s.endsWith("=") ? 1 : 0;
        byte[] buffer = new byte[s.length() * 3 / 4 - delta];
        int mask = 0xFF;
        int index = 0;
        for (int i = 0; i < s.length(); i += 4) {
            int c0 = toInt[s.charAt(i)];
            int c1 = toInt[s.charAt(i + 1)];
            buffer[index++] = (byte) (((c0 << 2) | (c1 >> 4)) & mask);
            if (index >= buffer.length) {
                return buffer;
            }
            int c2 = toInt[s.charAt(i + 2)];
            buffer[index++] = (byte) (((c1 << 4) | (c2 >> 2)) & mask);
            if (index >= buffer.length) {
                return buffer;
            }
            int c3 = toInt[s.charAt(i + 3)];
            buffer[index++] = (byte) (((c2 << 6) | c3) & mask);
        }
        return buffer;
    }

    public static String getParseString(String s, String after, String token1, String token2) {
        if (s == null || token1 == null || token2 == null)
            return null;
        int n1 = after != null ? s.indexOf(after) : 0;
        if (n1 < 0)
            return null;
        int n2 = s.indexOf(token1, n1);
        if (n2 < 0)
            return null;
        n2 += token1.length();
        int n3 = s.indexOf(token2, n2);
        if (n3 < 0)
            return null;
        return s.substring(n2, n3);
    }

    /**
     * Buffer size when reading from input stream.
     * @since ostermillerutils 1.00.00
     */
    private static int BUFFER_SIZE = 1024;

//    public static int BATCH_SIZE = 100*BUFFER_SIZE;

    /**
     * Copy the data from the input stream to the output stream.
     * @param in data source
     * @param out data destination
     * @throws IOException in an input or output error occurs
     * @since ostermillerutils 1.00.00
     */
    public static long copy(InputStream in, OutputStream out) throws IOException {
        long result = 0;
        byte[] buffer = new byte[BUFFER_SIZE];
        int read;
        while((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            result += read;
        }
        return result;
    }

    /**
     * Copy the data from the input stream to the output stream.
     * @param in data source
     * @param out data destination
     * @throws IOException in an input or output error occurs
     * @since ostermillerutils 1.00.00
     */
    public static long copy(InputStream in, OutputStream out, long chunkSize, OnLoadProgressListener listener) throws IOException, InterruptedException {
        if (listener == null)
            return copy(in, out);

        long result = 0;
        long next = chunkSize;
        byte[] buffer = new byte[BUFFER_SIZE];
        int read;
//        try {
        while((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            result += read;
            if (result >= next) {
                listener.onProgress(result);
                next += chunkSize;
            }
        }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return result;
    }

    public static void addGetParam(StringBuilder link, String name, String value) throws UnsupportedEncodingException {
        if (value == null)
            return;
        if (link.indexOf("?") >= 0)
            link.append("&");
        else
            link.append("?");
        link.append(URLEncoder.encode(name, "UTF-8")).append('=').append(URLEncoder.encode(value, "UTF-8"));
    }

    public static InputStream getInputEncoding(URLConnection connection) throws IOException {
        InputStream in;
        String encoding = connection.getContentEncoding();
        if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
            in = new GZIPInputStream(connection.getInputStream());
        } else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
            in = new InflaterInputStream(connection.getInputStream(), new Inflater(true));
        } else {
            in = connection.getInputStream();
        }
        return in;
    }

    public static String getStringFromStream(InputStream in, String charset) throws IOException {
        InputStreamReader isr;
        if (charset == null)
            isr = new InputStreamReader(in);
        else
            isr = new InputStreamReader(in, charset);
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder str = new StringBuilder();
        String line;
        boolean notFirst = false;
        while ((line = reader.readLine()) != null) {
            if (notFirst)
                str.append('\n');
            str.append(line);
            notFirst = true;
        }
        return str.toString();
    }

    public static void closeQuietly(Closeable in) {
        if (in != null)
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}

