package org.vadel.common;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

/**
 * Created by vbabin on 27.04.2018.
 */
public class URLParamEncoder {

    public static String encode(String input) throws UnsupportedEncodingException {
        StringBuilder resultStr = new StringBuilder();
        for (char ch : input.toCharArray()) {
            if (isUnsafe(ch)) {
                final byte[] bytes = ("" + ch).getBytes("UTF-8");

                for (byte b : bytes)
                {
                    resultStr.append('%');

                    int upper = (((int) b) >> 4) & 0xf;
                    resultStr.append(Integer.toHexString(upper).toUpperCase(Locale.US));

                    int lower = ((int) b) & 0xf;
                    resultStr.append(Integer.toHexString(lower).toUpperCase(Locale.US));
                }
//                resultStr.append('%');
//                resultStr.append(toHex(ch / 16));
//                resultStr.append(toHex(ch % 16));
            } else {
                resultStr.append(ch);
            }
        }
        return resultStr.toString();
    }

    private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }

    private static boolean isUnsafe(char ch) {
        if (ch > 128 || ch < 0)
            return true;
        return " %$&+,/:;=?@<>#%".indexOf(ch) >= 0;
    }
}
