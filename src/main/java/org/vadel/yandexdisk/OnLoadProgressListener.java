package org.vadel.yandexdisk;

/**
 * Created by vbabin on 26.04.2018.
 */
public interface OnLoadProgressListener {
    void onProgress(long progress) throws InterruptedException;
}
