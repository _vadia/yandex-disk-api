package org.vadel.yandexdisk.webdav;

import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.common.Helpers;
import org.vadel.common.URLParamEncoder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

public class WebDavFile {

    public static String convertPath(String path, boolean isDir) {
        // Remove prefix "disk:"
        if (path.startsWith("disk:")) {
            path = path.substring(5);
        }
        if (isDir) {
            // If folder Add backslash like: "/folder1/sub_folder/"
            path = Helpers.addBackSlash(path);
        }
        // WebDav version encoding "/folder1/document file 1.doc" -> "/folder1/document%20file%201.doc"
        try {
            path = URLParamEncoder.encode(path).replace("%2F", "/");
//            path = URLEncoder.encode(path, "UTF-8").replace("+", "%20").replace("%2F", "/");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static final String TYPE_DIR = "dir";

    public String name;
	
	public String path;
	
	public boolean isDir;

	public long date;
	
	public long fileLength;
	
	public String contentType;

	public String mediaType;

	public String downloadUrl;

	public String preview;

	public WebDavFile() {

	}

	public WebDavFile(JSONObject o) throws JSONException, UnsupportedEncodingException {
        this.name = o.getString("name");
        this.isDir = TYPE_DIR.equals(o.getString("type"));
        this.path = convertPath( o.getString("path"), this.isDir);

        this.date = Helpers.parseAtomDate(o.getString("created"));
        if (o.has("size"))
            this.fileLength = o.getLong("size");
        if (o.has("mime_type"))
            this.contentType = o.getString("mime_type");
        if (o.has("media_type"))
            this.mediaType = o.getString("media_type");
        if (o.has("file"))
            this.downloadUrl = o.getString("file");
        if (o.has("preview"))
            this.preview = o.getString("preview");
	}

	@Override
	public String toString() {
		return name + ":" + path + ":" + isDir + ":" + (new Date(date)) + ":" + fileLength / 1024 + ":" + contentType;
	}
	
	public String getParentPath() {
		if (path == null || path.length() == 1)
			return null;
		int i = path.lastIndexOf('/');
		if (i < 0)
			return null;
		if (i == path.length() - 1)
			i = path.lastIndexOf('/', i - 1);
		return path.substring(0, i + 1);
	}
}
