package org.vadel.yandexdisk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.common.Helpers;
import org.vadel.yandexdisk.authorization.BasicAuthorization;
import org.vadel.yandexdisk.authorization.OAuthAuthorization;
import org.vadel.yandexdisk.webdav.WebDavFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class YandexDiskApi {

    public static boolean DEBUG = false;
    
    private static final String PARSE_TOKEN = "#access_token=";
    

    public static final String API_URI = "https://cloud-api.yandex.net/v1/disk/resources";

    private static final String COPY_URI     = API_URI + "/copy";
    private static final String MOVE_URI     = API_URI + "/move";
    private static final String UPLOAD_URI   = API_URI + "/upload";
    private static final String DOWNLOAD_URI = API_URI + "/download";

    private static final String WEBDAV_URI    = "https://webdav.yandex.ru";
    private static final String USER_INFO_URI = WEBDAV_URI + "/?userinfo";

    protected static final String BASE_OAUTH_AUTHORIZE_URL = 
        "https://oauth.yandex.ru/authorize?response_type=token&client_id=";

    private static final String PUT    = "PUT";
    private static final String GET    = "GET";
    private static final String POST   = "POST";
    private static final String DELETE = "DELETE";

    public static int BUFFER = 1024;

    public static final int HUNDRED_KB   = 100*1024;
    public static final int ONE_MB   = 1024*1024;
    public static final int TEN_MB   = 10*ONE_MB;
    public static final int FIFTY_MB = 50*ONE_MB;
    
    private long chunkSize = ONE_MB;
    
    protected final String clientId;

    public final Inner inner = new Inner();

    // final HttpClient client = HttpClientBuilder.create().build();
//    HttpClient client = new DefaultHttpClient();

    public YandexDiskApi(String clientId) {
        this.clientId = clientId;
    }
    
    public String getCredentialsLogin() {
        if (inner.auth instanceof BasicAuthorization)
            return ((BasicAuthorization) inner.auth).login;
        else
            return null;
    }

    public String getCredentialsPassword() {
        if (inner.auth instanceof BasicAuthorization)
            return ((BasicAuthorization) inner.auth).pass;
        else
            return null;
    }

    public void setCredentials(String login, String pass) {
        inner.auth = new BasicAuthorization(login, pass);
    }
    
    public String getOAthRequestUrl() {
        return BASE_OAUTH_AUTHORIZE_URL + clientId;
    }
    
    public String getToken() {
        if (inner.auth instanceof OAuthAuthorization)
            return ((OAuthAuthorization) inner.auth).token;
        else
            return null;
    }
    
    public void setToken(String value) {
        inner.auth = new OAuthAuthorization(value);
    }
    
    /**
     * Response example: http://aaaaa.aaaaa.com/callback#access_token=00a11b22c3333c44a7e7d6db623bd5e0&token_type=bearer&state=
     * @param uri - Response uri
     */
    public void setTokenFromCallBackURI(String uri) {
        int i1 = uri.indexOf(PARSE_TOKEN);
        if (i1 < 0)
            return;
        i1 += PARSE_TOKEN.length();
        int i2 = uri.indexOf("&", i1);
        if (i2 < 0)
            return;
        setToken(uri.substring(i1, i2));
    }

    public boolean isAuthorization() {
        return inner.auth != null && inner.auth.isValid();
    }

    public long getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(long value) {
        if (value <= 0)
            return;
        chunkSize = Math.max(value, HUNDRED_KB);
    }

    public String getUserLogin() {
        if (inner.auth instanceof BasicAuthorization) {
            return ((BasicAuthorization) inner.auth).login;
        } else if (inner.auth instanceof OAuthAuthorization) {
            return userLogin();
        } 
        return null;
    }

    public String userRaw() {
        return inner.executeString(GET, USER_INFO_URI, null);
    }

    public String userLogin() {
        String s = userRaw();
        if (s == null)
            return null;
        String[] ss = s.split("\n");
        for (String ln: ss) {
            if (ln.startsWith("login:")) {
                return ln.substring(6);
            }
        }
        return s;
    }

    public String userName() {
        String s = userRaw();
        if (s == null)
            return null;

        String[] ss = s.split("\n");
        StringBuilder name = new StringBuilder();
        for (String ln: ss) {
            if (ln.startsWith("fio:")) {
                return ln.substring("fio:".length());
            } else if (ln.startsWith("firstname")) {
                if (name.length() > 0)
                    name.append(' ');
                name.append(ln.substring("firstname:".length()));
            } else if (ln.startsWith("lastname")) {
                if (name.length() > 0)
                    name.append(' ');
                name.append(ln.substring("lastname:".length()));
            }
        }
        return name.toString();
    }

    /**
     * PUT
     * https://cloud-api.yandex.net/v1/disk/resources
     *   ? path=<путь к создаваемой папке>
     *   & [fields=<свойства, которые нужно включить в ответ>]
     * @param path
     * @return
     */
    public boolean createFolder(String path) {
        JSONObject obj = executeJsonPath(PUT, API_URI, path);
        return obj != null && obj.has("href");
    }

    /**
     * DELETE
     * https://cloud-api.yandex.net/v1/disk/resources
     *    ? path=<путь к удаляемому ресурсу>
     *    & [permanently=<признак безвозвратного удаления>]
     *    & [fields=<свойства, которые нужно включить в ответ>]
     * @param path
     * @return
     */
    public boolean delete(String path) {
        JSONObject obj = executeJsonPath(DELETE, API_URI, path);
        return obj != null && obj.has("href");
    }

    /**
     * POST
     * https://cloud-api.yandex.net/v1/disk/resources/copy
     *   ? from=<путь к копируемому ресурсу>
     *   & path=<путь к скопированному ресурсу>
     *   & [overwrite=<признак перезаписи>]
     *   & [fields=<свойства, которые нужно включить в ответ>]
     * @param src
     * @param dst
     * @return
     */
    public boolean copy(String src, String dst) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("from", src);
        params.put("path", dst);
        JSONObject obj = inner.executeJson(POST, COPY_URI, params);// getDownloadUrl(path);
        return obj != null && obj.has("href");
    }

    /**
     * POST
     * https://cloud-api.yandex.net/v1/disk/resources/move
     *    ? from=<путь к перемещаемому ресурсу>
     *    & path=<путь к перемещенному ресурсу>
     *    & [overwrite=<признак перезаписи>]
     *    & [fields=<свойства, которые нужно включить в ответ>]
     * @param src
     * @param dst
     * @return
     */
    public boolean move(String src, String dst) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("from", src);
        params.put("path", dst);
        JSONObject obj = inner.executeJson(POST, MOVE_URI, params);// getDownloadUrl(path);
        return obj != null && obj.has("href");
    }

    /**
     * GET
     * https://cloud-api.yandex.net/v1/disk/resources
     *    ? path=<путь к ресурсу>
     *    & [fields=<свойства, которые нужно включить в ответ>]
     *    & [limit=<ограничение на количество возвращаемых ресурсов>]
     *    & [offset=<смещение относительно начала списка>]
     *    & [preview_crop=<признак обрезки превью>]
     *    & [preview_size=<размер превью>]
     *    & [sort=<атрибут сортировки>]
     * @param path
     * @return Json items like
     * {
     *      "name": "Folder_ABC",
     *      "exif": {},
     *      "resource_id": "141089445:ebb39edac81b52e3611bde16e0d5f239a3760905b9195aa3b03d7d02efb5152a",
     *      "created": "2016-09-03T05:37:37+00:00",
     *      "modified": "2016-09-03T05:37:37+00:00",
     *      "path": "disk:/folder1/Folder_ABC",
     *      "comment_ids": {
     *        "private_resource": "141089445:ebb39edac81b52e3611bde16e0d5f239a3760905b9195aa3b03d7d02efb5152a",
     *        "public_resource": "141089445:ebb39edac81b52e3611bde16e0d5f239a3760905b9195aa3b03d7d02efb5152a"
     *      },
     *      "type": "dir",
     *      "revision": 1472881057356667
     * }
     */
    public JSONArray files(String path) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("limit", "1000");
        params.put("path", path);
        JSONObject obj = inner.executeJson(GET, API_URI, params);
        if (obj == null || !obj.has("_embedded"))
            return null;
        try {
            JSONObject embedded = obj.getJSONObject("_embedded");
            return embedded.getJSONArray("items");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<WebDavFile> filesOld(String path) {
        JSONArray items = files(path);
        if (items == null)
            return null;
        List<WebDavFile> res = new ArrayList<WebDavFile>();
        try {
            for (int i = 0; i < items.length(); i++) {
                JSONObject o = items.getJSONObject(i);
                WebDavFile file = new WebDavFile(o);
                res.add(file);
            }
            return res;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * GET
     * https://cloud-api.yandex.net/v1/disk/resources/upload
     *    ? path=<путь, по которому следует загрузить файл>
     *    & [overwrite=<признак перезаписи>]
     *    & [fields=<свойства, которые нужно включить в ответ>]
     * @param path
     * @param data
     * @return
     */
    protected boolean upload(String path, Object data) {
        JSONObject obj = executeJsonPath(GET, UPLOAD_URI, path);
        if (obj == null)
            return false;
        try {
            inner.execute(obj.getString("method"), obj.getString("href"), null, null, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean uploadFile(String path, InputStream data, long fileLength) {
        return upload(path, data);
    }


    public boolean uploadFile(String path, String data) throws UnsupportedEncodingException {
        return upload(path, data);
    }

    /**
     * Only OAuth authorization
     * GET
     * https://cloud-api.yandex.net/v1/disk/resources/download
     *    ? path=<путь к скачиваемому файлу>
     *    & [fields=<свойства, которые нужно включить в ответ>]
     * @param path - path to file in yandex disk
     * @return Download url or null
     */
    public String getDownloadUrl(String path) {
        JSONObject obj = executeJsonPath(GET, DOWNLOAD_URI, path);// getDownloadUrl(path);
        if (obj != null)
            try {
                return obj.getString("href");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return null;
    }

    public InputStream getFileInputStream(String path, long start) {
        String downloadUrl = getDownloadUrl(path);
        if (downloadUrl == null)
            return null;
        HashMap<String, String> params = null;
        if (start > 0) {
            params = new HashMap<String, String>();
            params.put("Range", "bytes=" + String.valueOf(start) + "-");
        }
        return inner.execute(GET, downloadUrl, null, params, null);
    }

    public String getFileString(String path) {
        return getFileString(path, 0);
    }

    public String getFileString(String path, long start) {
        String downloadUrl = getDownloadUrl(path);
        if (downloadUrl == null)
            return null;

        HashMap<String, String> params = null;
        if (start > 0) {
            params = new HashMap<String, String>();
            params.put("Range", "bytes=" + String.valueOf(start) + "-");
        }
        try {
            return Helpers.getStringFromStream(inner.execute(GET, downloadUrl, params, null, null), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long downloadFile(String path, FileOutputStream fos, long start,
                             OnLoadProgressListener listener) throws InterruptedException {
        InputStream in = getFileInputStream(path, start);
        if (in == null)
            return 0;

        try {
            if (listener != null) {
                return start + Helpers.copy(in, fos, chunkSize, listener);
            } else {
                return start + Helpers.copy(in, fos);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Helpers.closeQuietly(in);
        }
        return 0;
    }

    protected JSONObject executeJsonPath(String method, String url, String path) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("path", path);
        return inner.executeJson(method, url, params);// getDownloadUrl(path);
    }

}
