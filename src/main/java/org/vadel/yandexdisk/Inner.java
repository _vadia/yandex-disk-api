package org.vadel.yandexdisk;

import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.common.Helpers;
import org.vadel.yandexdisk.authorization.Authorization;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Created by vbabin on 26.04.2018.
 */
public class Inner {

    public Authorization auth;

    public String getAuthorization() {
        if (auth != null && auth.isValid())
            return auth.getAuthorizationHeader();
        else
            return null;
    }

    public InputStream execute(String method, String url, Map<String, String> params, Map<String, String> headers, Object data) {
        try {
            URL Url;
            if (params != null) {
                StringBuilder str = new StringBuilder(url);
                for (String name : params.keySet()) {
                    Helpers.addGetParam(str, name, params.get(name));
                }
                Url = new URL(str.toString());
            } else {
                Url = new URL(url);
            }

            HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
            if (conn == null)
                return null;

            conn.setRequestMethod(method);
            conn.setInstanceFollowRedirects(true);
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            conn.addRequestProperty("Authorization", getAuthorization());
            if (headers != null) {
                for (String name : headers.keySet()) {
                    conn.addRequestProperty(name, headers.get(name));
                }
            }
            if (data instanceof String || data instanceof StringBuilder) {
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data.toString());
                wr.flush();
//            } else if (data is forsms) {
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(request.dataFormsAsString().toString());
//                wr.flush();
            } else if (data instanceof InputStream) {
                Helpers.copy((InputStream) data, conn.getOutputStream());
            }
            return Helpers.getInputEncoding(conn);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String executeString(String method, String url, Map<String, String> params) {
        InputStream in = execute(method, url, params, null, null);
        if (in == null)
            return null;
        try {
            return Helpers.getStringFromStream(in, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject executeJson(String method, String url, Map<String, String> params) {
        String s = executeString(method, url, params);
        if (s == null)
            return null;
        try {
            return new JSONObject(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }
}
