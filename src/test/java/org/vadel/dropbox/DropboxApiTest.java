package org.vadel.dropbox;

import junit.framework.Assert;
import org.junit.Test;
import org.vadel.common.Config;
import org.vadel.dropbox.model.AccountInfo;
import org.vadel.dropbox.model.DropFolder;
import org.vadel.dropbox.model.Metadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by VBabin on 17.10.2017.
 */
public class DropboxApiTest {

    public static final String TEST_FILE_PATH = "/Music/Сергей Тармашев - Катастрофа/000 Вступление.mp3";
    static String CLIENT_ID;
    static String REDIRECT_URI;

    static String ACCESS_TOKEN;
    static String UID;

    static {
        Properties local = Config.getLocalProperties();
        CLIENT_ID    = local.getProperty("dropbox-client-id");
        REDIRECT_URI = local.getProperty("dropbox-redirect-uri");
        ACCESS_TOKEN = local.getProperty("dropbox-access-token");
        UID          = local.getProperty("dropbox-uid");
        if (CLIENT_ID == null) {
            local.setProperty("dropbox-client-id", "");
            local.setProperty("dropbox-redirect-uri", "");
            Config.saveLocalProperties();
        } else if (ACCESS_TOKEN == null) {
            auth();
        }
    }

    static void auth() {
        Properties local = Config.getLocalProperties();
        DropboxApi dropbox = new DropboxApi(CLIENT_ID, REDIRECT_URI);
        dropbox.setEndPoint(DropboxApi.RootEndPoint.dropbox);
        String authUri = local.getProperty("dropbox-auth-uri");
        try {
            if (authUri == null) {
                System.out.println("Open in your browser: " + dropbox.getAuthoriztionUrl());
                authUri = readLine("Allow access and paste here redirected url: ");
                Assert.assertTrue(authUri != null && authUri.length() > 0);
            }
            dropbox.finishAuthoriztion(authUri);
            ACCESS_TOKEN = dropbox.getAccessToken();
            UID = dropbox.uid;
            local.setProperty("dropbox-access-token", ACCESS_TOKEN);
            local.setProperty("dropbox-uid", UID);
            Config.saveLocalProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static DropboxApi dropbox() {
        DropboxApi dropbox = new DropboxApi(CLIENT_ID, REDIRECT_URI);
        dropbox.setAccessToken(ACCESS_TOKEN, UID);
        dropbox.setEndPoint(DropboxApi.RootEndPoint.dropbox);
        return dropbox;
    }

    @Test
    public void testAuth() {
        assertTrue(CLIENT_ID != null && ACCESS_TOKEN != null);
    }

//    @Test
    public void testAccountInfo() {
        AccountInfo info = dropbox().accountInfo();
        assertNotNull(info);
    }

//    @Test
    public void testFiles() {
        DropFolder root = dropbox().listFolder("/", true);
        assertNotNull(root);
        assertTrue(root.contents.size() > 0);
    }

//    @Test
    public void testMedia() {
        String media = dropbox().media("/1.todo");
        assertNotNull(media);
    }

//    @Test
    public void testDownload() {
        String file = dropbox().getFileString("/1.todo");
        System.out.println(file);
        assertNotNull(file);
        assertTrue(file.length() > 0);
    }

////    @Test
    public void testStream() {
        assertNotNull(dropbox().getFileStream(TEST_FILE_PATH));
    }

//    @Test
    public void testMetadata() {
        Metadata data = dropbox().metadata("/Music");
        assertNotNull(data);
    }

    public static String readLine(String s) throws IOException {
        BufferedReader mReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(s);
        return mReader.readLine();
    }
}
