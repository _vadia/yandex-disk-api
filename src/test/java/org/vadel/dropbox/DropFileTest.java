package org.vadel.dropbox;

import junit.framework.Assert;
import org.junit.Test;
import org.vadel.dropbox.model.DropFile;

/**
 * Created by VBabin on 19.10.2017.
 */
public class DropFileTest {

    static DropFile file(String path) {
        DropFile dropFile = new DropFile();
        dropFile.path = path;
        return dropFile;
    }

    @Test
    public void testGetParent() {
        Assert.assertNull(file("/").getParent());
        Assert.assertNull(file("").getParent());
        Assert.assertNull(file(null).getParent());
        Assert.assertEquals("", file("/Music").getParent());
        Assert.assertEquals("/Music/Tupik", file("/Music/Tupik/1.txt").getParent());
        Assert.assertEquals("/Music/Tupik", file("/Music/Tupik/Folder").getParent());
//        Assert.assertEquals("", file("/Music").getParent());
//        Assert.assertEquals("", file("/Music").getParent());
    }
}
