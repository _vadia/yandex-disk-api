package org.vadel.cloudmailru;

import junit.framework.Assert;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.vadel.common.Config;
import org.vadel.common.Helpers;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by vbabin on 23.05.2018.
 */
public class CloudMailRuApiTest {

    static final String KEY_LOGIN    = "cloud.mail.ru-login";
    static final String KEY_PASSWORD = "cloud.mail.ru-password";
    static final String KEY_TOKEN    = "cloud.mail.ru-token";
    static final String KEY_COOKIES  = "cloud.mail.ru-cookies";

    static String login;
    static String password;
    static String accessToken;
    static String cookies;

    static {
        Properties local = Config.getLocalProperties();
        login       = local.getProperty(KEY_LOGIN);
        password    = local.getProperty(KEY_PASSWORD);
        accessToken = local.getProperty(KEY_TOKEN);
        cookies     = local.getProperty(KEY_COOKIES);
    }

    CloudMailRuApi auth() throws IOException {
        CloudMailRuApi api = new CloudMailRuApi();
        api.setAuth(login, password, accessToken);
        api.cookies.setCookie(cookies);
        if (!api.isAuthenticated()) {
            Assert.assertNotNull(login);
            Assert.assertNotNull(password);
            boolean res = api.authorize(login, password);
            Assert.assertTrue(res);
            Properties local = Config.getLocalProperties();
            local.setProperty(KEY_TOKEN, api.getAccessToken());
            local.setProperty(KEY_COOKIES, api.cookies.getCookie());
            Config.saveLocalProperties();
        }
        return api;
    }

//    @Test
    public void testAuth() throws IOException {
        CloudMailRuApi api = auth();
        Assert.assertTrue(api != null && api.isAuthenticated());
    }

//    @Test
    public void testDiskSpace() throws IOException {
        CloudMailRuApi api = auth();
        JSONObject o = api.diskSpace();
        Assert.assertTrue(o != null);
    }

//    @Test
    public void testItems() throws IOException {
        CloudMailRuApi api = auth();
        JSONArray o = api.items(api.root());
        Assert.assertTrue(o != null && o.length() > 0);
    }

//    @Test
    public void testCreateFolder() throws IOException {
        CloudMailRuApi api = auth();
        Assert.assertTrue(api.createFolder("/New folder"));
//        Assert.assertTrue(api.createFile("/File1"));
    }

//    @Test
    public void testCopy() throws IOException {
        CloudMailRuApi api = auth();
        String path = "/file.txt";
        String folder = "/New folder";
        Assert.assertTrue(api.upload(path, new StringBuilder("test file asdasd asdasd")));
        Assert.assertTrue(api.copyTo(path, folder));
        Assert.assertTrue(api.remove(path));
        Assert.assertTrue(api.remove(Helpers.addBackSlash(folder, path)));
    }

//    @Test
    public void testMove() throws IOException {
        CloudMailRuApi api = auth();
        String path = "/file.txt";
        String folder = "/New folder";
        Assert.assertTrue(api.upload(path, new StringBuilder("test file asdasd asdasd")));
        Assert.assertTrue(api.moveTo(path, folder));
        Assert.assertTrue(api.remove(Helpers.addBackSlash(folder,path)));
    }

//    @Test
    public void testRename() throws IOException {
        CloudMailRuApi api = auth();
        String path = "/file.txt";
        String name2 = "file2.txt";
        Assert.assertTrue(api.upload(path, new StringBuilder("test file asdasd asdasd")));
        Assert.assertTrue(api.rename(path, name2));
        Assert.assertTrue(api.remove("/" + name2));
    }

//    @Test
    public void testUpload() throws IOException {
        CloudMailRuApi api = auth();
        String path = "/file.txt";
        Assert.assertTrue(api.upload(path, new StringBuilder("test file asdasd asdasd")));
        Assert.assertTrue(api.remove(path));
    }

    static final String TEST_FILE = "16719_таблица_сопряжения.txt";

//    @Test
    public void testGetFileString() throws IOException, InterruptedException {
        CloudMailRuApi api = auth();
        String s = api.getFileString("/" + TEST_FILE, "windows-1251");
        Assert.assertNotNull(s);
    }

//    @Test
    public void testGetPublic() throws IOException, InterruptedException {
        CloudMailRuApi api = auth();
        String path = "/" + TEST_FILE;
        String publicUrl = api.makePublic(path);
        Assert.assertNotNull(publicUrl);
        String downloadUrl = api.getDownloadLink(publicUrl);
        Assert.assertNotNull(downloadUrl);
        Assert.assertTrue(api.makeUnpublic(publicUrl, path) );
    }

//    @Test
    public void testGetDownloadLink() throws IOException {
        CloudMailRuApi api = auth();
        String url = api.getDownloadLink("/" + TEST_FILE);
        Assert.assertNotNull(url);
    }

//    @Test
    public void testDownload() throws IOException, InterruptedException {
        CloudMailRuApi api = auth();
        File dst = new File(TEST_FILE);
        long size = api.download("/" + TEST_FILE, dst, null);
        Assert.assertTrue(size > 0);
    }
}
