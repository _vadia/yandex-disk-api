package org.vadel.common;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by vbabin on 24.05.2018.
 */
public class HelpersTest {

    @Test
    public void testAddBackSlash() {
        Assert.assertEquals("/aaa/", Helpers.addBackSlash("/aaa"));
        Assert.assertEquals("/aaa/bbb", Helpers.addBackSlash("/aaa", "bbb"));
        Assert.assertEquals("/aaa/bbb", Helpers.addBackSlash("/aaa", "/bbb"));
        Assert.assertEquals("/aaa/bbb", Helpers.addBackSlash("/aaa/", "/bbb"));
    }
}
