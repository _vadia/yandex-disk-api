package org.vadel.common;

import java.io.*;
import java.util.Date;
import java.util.Properties;

/**
 * Created by VBabin on 17.10.2017.
 */
public class Config {

    public static final String FILE_PROPERTIES  = "./config.properties";

    public static final String LOCAL_PROPERTIES = "./local.properties";

    private static Properties props, local;

    private static Properties init(Properties props, String file) {
        if (props != null)
            return props;
        File config = new File(file);
        if (!config.exists())
            return null;
        props = new Properties();
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(config);
            props.load(fin);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeQuietly(fin);
        }
        return props;
    }

    public static Properties getProperties() {
//		props = init(props, FILE_PROPERTIES);
//		return props;
        return props = init(props, FILE_PROPERTIES);
    }

    public static Properties getLocalProperties() {
        local = init(local, LOCAL_PROPERTIES);
        if (local == null)
            local = new Properties();
        return local;
    }

    public static void saveLocalProperties() {
        if (local == null)
            return;
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(LOCAL_PROPERTIES);
            local.store(fout, "Auto save properties: " + new Date());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeQuietly(fout);
        }
    }

    static void closeQuietly(Closeable cl) {
        if (cl == null)
            return;
        try {
            cl.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
