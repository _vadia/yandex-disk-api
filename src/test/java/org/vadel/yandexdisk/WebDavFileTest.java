package org.vadel.yandexdisk;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.vadel.yandexdisk.webdav.WebDavFile;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import static junit.framework.Assert.*;

public class WebDavFileTest {

	static String DIR0 = "/";
	static String DIR1 = DIR0 + "audiobooks/";
	static String DIR2 = DIR1 + "book1/";
	
	@Test
	public void testGetParetn() {
		WebDavFile dir = new WebDavFile();
		dir.path = DIR2;
		dir.path = dir.getParentPath();
		assertEquals(DIR1, dir.path);
		dir.path = dir.getParentPath();
		assertEquals(DIR0, dir.path);
	}

	static String JSON_FILE_OBJ = "{\"antivirus_status\": \"clean\",\"sha256\": \"37f68def919ddb194ce172cacbb3a7e00a991352e90357eec7bd27f7ecc5c323\",\"name\": \"0092.ЭПИЛОГ-03.mp3\",\"exif\": {},\"created\": \"2018-04-19T18:09:15+00:00\",\"revision\": 1524161355254246,\"resource_id\": \"141089445:46b717d96b3f60becd53f424a9a8a11b0312b09d5b3006dbcc93bb7f3176070a\",\"modified\": \"2018-04-19T18:09:15+00:00\",\"comment_ids\": {\"private_resource\": \"141089445:46b717d96b3f60becd53f424a9a8a11b0312b09d5b3006dbcc93bb7f3176070a\",\"public_resource\": \"141089445:46b717d96b3f60becd53f424a9a8a11b0312b09d5b3006dbcc93bb7f3176070a\"},\"file\": \"https://downloader.disk.yandex.ru/disk/0b51fcdb72a8b7f32f406863e798227c013a17feceec0de8951822d02a8f7019/5ae2ee2e/dpy6XelAIJZO-wbQgvLUykJXyEVBI0-7APX6R4K6R4wWumhdmZIcQWM7LNaffOXfXxRfAwMK3U2qOdfZXURFzw%3D%3D?uid=141089445&filename=0092.%D0%AD%D0%9F%D0%98%D0%9B%D0%9E%D0%93-03.mp3&disposition=attachment&hash=&limit=0&content_type=audio%2Fmpeg&fsize=3875264&hid=f11f924ae26ac9eb8eea402546876967&media_type=audio&tknv=v2&etag=f08bfa46ca2ca25dc45d379bc9b83470\",\"media_type\": \"audio\",\"path\": \"disk:/audiobooks/Panov Vadim - Anklavy. Moskovskii klub/0092.ЭПИЛОГ-03.mp3\",\"md5\": \"f08bfa46ca2ca25dc45d379bc9b83470\",\"type\": \"file\",\"mime_type\": \"audio/mpeg\",\"size\": 3875264}";

	@Test
	public void testFromJson() throws JSONException, UnsupportedEncodingException {
		WebDavFile file = new WebDavFile(new JSONObject(JSON_FILE_OBJ));
		assertEquals("Wrong file name parsing", "0092.ЭПИЛОГ-03.mp3", file.name);
		assertFalse("Wrong parse is_dir status", file.isDir);
		assertEquals("Wrong parse file size", 3875264, file.fileLength);
		assertEquals("Wrong parse content type", "audio/mpeg", file.contentType);
//		Date d = new Date(file.date);
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(file.date);
		assertEquals("Wrong file date parsing", 2018, c.get(Calendar.YEAR));
		assertEquals("File path back compatibility converting incorrect", "/audiobooks/Panov%20Vadim%20-%20Anklavy.%20Moskovskii%20klub/0092.%D0%AD%D0%9F%D0%98%D0%9B%D0%9E%D0%93-03.mp3", file.path);
	}

    final static String TEST_PATH1 = "/audiobooks/(Audiobook_Rus_2009)_Geiman_Amerikanskie_Bogi_by_Lutz_Records/part1/";

    final static String TEST_PATH2_SRC = "/audiobooks/Panov Vadim - Anklavy. Moskovskii klub/0092.ЭПИЛОГ-03.mp3";
    final static String TEST_PATH2_DST = "/audiobooks/Panov%20Vadim%20-%20Anklavy.%20Moskovskii%20klub/0092.%D0%AD%D0%9F%D0%98%D0%9B%D0%9E%D0%93-03.mp3";

    @Test
    public void testPathConvert() {
        assertEquals("Wrong path convert 1", TEST_PATH1, WebDavFile.convertPath(TEST_PATH1, true));
        assertEquals("Wrong path convert 2", TEST_PATH2_DST, WebDavFile.convertPath(TEST_PATH2_SRC, false));
    }
}
